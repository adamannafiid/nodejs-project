const express = require('express');
const mysql = require('mysql');

const app = express();

app.use(express.static('public'));
app.use(express.urlencoded({extended: false}));
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'password',
  database: 'document_services'
});


connection.connect((err) => {
    if (err) {
      console.log('error connecting: ' + err.stack);
      return;
    }
    console.log('success');
  });

  app.get('/', (req, res) => {
    res.render('top.ejs');
  });
  
  
  app.get('/index', (req, res) => {
    connection.query(
      'SELECT * FROM document',
      (error, results) => {
        console.log(results);
        res.render('index.ejs', {document: results});
      }
    );
  });

  
app.get('/new', (req, res) => {
  res.render('new.ejs');
});

app.post('/create', (req, res) => {
  connection.query(
    'INSERT INTO document (name, type, folder_id, content, timestamps, owner_id, share, company_id) VALUES (?)',
    [req.body.itemName],
    (error, results) => {
      res.redirect('/index');
    }
  );
});

app.post('/delete/:id', (req, res) => {
  connection.query(
    'DELETE FROM document WHERE id = ?',
    [req.params.id],
    (error, results) => {
      res.redirect('/index');
    }
  );
});

app.get('/edit/:id', (req, res) => {
  connection.query(
    'SELECT * FROM document WHERE id = ?',
    [req.params.id],
    (error, results) => {
      res.render('edit.ejs', {document: results[0]});
    }
  );
});

app.post('/update/:id', (req, res) => {
  // Ketik code untuk memperbarui item yang dipilih
  connection.query('UPDATE document set name=?, type=?, folder_id=?, content=?, timestamps=?, owner_id=?, share=?, company_id=? where id=?',[req.body.itemName,req.body.itemType,req.body.itemFolder,req.body.itemContent,req.body.itemTime,req.body.itemOwner,req.body.itemShare,req.body.itemCompany,req.params.id], (error, results) =>{
    res.redirect('/index');
    
  });
  // Hapus code pengalihan ke halaman daftar
});



  app.listen(3020);

